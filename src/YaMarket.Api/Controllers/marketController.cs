using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YaMarket.Repository;
using YaMarket.Repository.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace YaMarket.Api.Controllers
{
   
    public class MarketController : Controller
    {
        [HttpGet("{region}/offers/diff")]
        public async Task<ActionResult> Diff(string region)
        {
            var _region = region == "spb"? 1 : 0;
            var product = await ProductService.DiffOffers( _region, DateTime.Now.AddMinutes(-5));
            if (product == null || !product.Any())
                return NotFound();

            return Ok(product);
        }

        [HttpGet("{region}/offers/")]
        public async Task<ActionResult> List(string region)
        {
            var _region = region == "spb"? 1 : 0;
            var product = await ProductService.ListOffers(_region);
            if (product == null || !product.Any())
                return NotFound();

            return Ok(product);
        }

        //[HttpDelete("{region}/offers/")]
        //public async Task<ActionResult> Post(string region)
        //{
        //    var _region = region == "spb"? 1 : 0;
        //    //await ProductService.ResetAll(_region);
        //    //await TaskService.ResetAll( _region);
            
        //    return Ok();
        //}

        [HttpGet("{region}/offers/{id:int}")]
        public async Task<ActionResult> Get(string region, int id)
        {
            var _region = region == "spb"? 1 : 0;
            var product = await ProductService.GetOffers(id, _region);
            if (product == null)
                return NotFound("Offer is not registered");
            if (!product.Any())
                return NotFound("Offers not found");

            return Ok(product);
        }

        [HttpGet("{region}/offers/mvideo/{id:int}")]
        public async Task<ActionResult> GetMvideo(string region, int id)
        {
            var _region = region == "spb" ? 1 : 0;
            var product = await ProductService.GetOffersMvideo(id, _region);
            if (product == null)
                return NotFound("Offer is not registered");
            if (!product.Any())
                return NotFound("Offers not found");

            return Ok(product);
        }

        [HttpPost("{region}/offers/{id:int}")]
        public async Task<ActionResult> Post(string region, int id)
        {           
            var _region = region == "spb"? 1 : 0;

            //await TaskService.InsertSoft(id, _region);           

            var product = await ProductService.GetOffers(id, _region);
            if (product == null || !product.Any())
                return NotFound();

            return Ok(product);
        }
        [HttpGet("update_offers/")]
        public async Task<ActionResult> Post()
        {
            var task =  AppConfig.ApiUrls.Select(url => new EmarketApiClient().GetIds(url, CancellationToken.None));
            var result = (await Task.WhenAll(task)).SelectMany(s => s).Where(g=> g.city == "spb").GroupBy(g=> new { g.marketId, g.city}).Select(s=>s.Key).ToArray();
            Parallel.ForEach(Partitioner.Create(0, result.Count()), (range) =>
            {
                for (long i = range.Item1; i < range.Item2; )
                {
                    try
                    {
                        TaskService.InsertSoft(int.Parse(result[i].marketId), result[i].city == "spb" ? 1 : 0).Wait();
                        i++;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Task.Delay(TimeSpan.FromMilliseconds(10)).Wait();
                        
                    }
                    
                }
                    
            });

            return Ok(result.Count());
        }
    }
}
