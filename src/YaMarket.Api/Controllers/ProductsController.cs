﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YaMarket.Repository.Models;
using YaMarket.Repository.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace YaMarket.Controllers
{
    public class ProductsController : Controller
    {
        // GET: api/values
        [HttpGet("{region:enum(YaMarket.Region)}/products")]
        public async Task<List<Product>> Get(Region region, int page = 1, int size = 20, int? offset = null)
        {
            if (page < 1) page = 1;
            int _offset = (page - 1) * size;
            if (offset.HasValue) _offset = offset.Value;
            return await ProductService.List((int)region,_offset, size);
        }

        // GET api/values/5
        [HttpGet("{region:enum(YaMarket.Region)}/products/{id:int}")]
        public async Task<ActionResult> Get(Region region, int id)
        {
            var product = await ProductService.Get(id, (int)region);
            if (product == null)
                return NotFound();

            return Ok(product);
        }

        [HttpPost("{region:enum(YaMarket.Region)}/products/{id:int}")]
        public async Task<ActionResult> Post(Region region, int id)
        {
            var product = await ProductService.Get(id, (int)region);
            if (product == null)
                return NotFound();

           // await TaskService.Insert(product.market_id, (int)region, id);
            product = await ProductService.Get(id,(int)region);

            return Ok(product);
        }



        // POST api/values
        [HttpPost("{region:enum(YaMarket.Region)}/products")]
        public async Task<Product> Post([FromRoute] Region region, [FromBody]Product value)
        {
            //await TaskService.Insert(value.market_id,(int)region,value.id);
            return await ProductService.Get(value.id, (int)region);
        }
    }
}
