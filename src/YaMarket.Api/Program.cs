﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using YaMarket.Worker;

namespace YaMarket
{

    public class Program
    {
        public static StaticQueue Queue = StaticQueue.Instance;
        public static void Main(string[] args)
        {

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            if(config.GetValue("WORKER", false))
                Queue.RunJobs(config.GetValue("WORKER_COUNT", 10));

            var host = new WebHostBuilder()
               .UseConfiguration(config)
               .UseKestrel()
               .UseUrls("http://0.0.0.0:5000")
               .UseContentRoot(Directory.GetCurrentDirectory())
               .UseIISIntegration()
               .UseStartup<Startup>()
               .Build();
            
            host.Run();
            Queue.Stop();
        }
    }
}
