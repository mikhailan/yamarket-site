﻿using Apix.Http.Client;

namespace YaMarket.Worker
{
    public class TokenSettings
    {
        public string UID { get; set; }
        public string Token { get; set; }
        public ProxySettings Proxy { get; set; }
    }
}