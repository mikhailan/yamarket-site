﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Apix.Extensions;
using Apix.Http.Client;
using Apix.Sync.YaMarket;
using Apix.Sync.YaMarket.Models;
using Yamarket.Client;
using YaMarket.Repository;
using YaMarket.Repository.Infrastructure;
using YaMarket.Repository.Models;
using YaMarket.Repository.Services;


namespace YaMarket.Worker
{
    public class StaticQueue
    {

        #region Fields

        private static ConcurrentQueue<QueueItem> DownloadItemsQueue = new ConcurrentQueue<QueueItem>();
        private static ConcurrentQueue<ProxySettings> ProxyItemsQueue = new ConcurrentQueue<ProxySettings>();
        private static ConcurrentBag<ProxySettings> DisableProxy = new ConcurrentBag<ProxySettings>();
        private static readonly ConcurrentDictionary<int, QueueItem> WorkedItems = new ConcurrentDictionary<int, QueueItem>();
        private static readonly Lazy<StaticQueue> Lazy = new Lazy<StaticQueue>(() => new StaticQueue());
        private static Timer _newTaskTimer;
        private static Timer _proxyUpdaterTimer;
        private static List<Task> _workerTimers;
        private bool _isRun;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        private static readonly object mutex = new object();
        static volatile TokenSettings _token;    // GOOD: Thread-safe

        #endregion

        #region Properties

        public static StaticQueue Instance => Lazy.Value;
        public static int Delay { get; set; }

        public static TokenSettings Token
        {
            get
            {
                if (_token == null)
                {
                    lock (mutex)
                    {
                        if (_token == null)
                            _token = GetTokenSettings();
                    }
                }
                return _token;
            }
            set => _token = value;
        }
        #endregion

        private StaticQueue()
        {

            _newTaskTimer = new Timer(GetNewTask, null, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10));
            _proxyUpdaterTimer = new Timer(UpdateProxy, null, TimeSpan.FromSeconds(5), TimeSpan.FromMinutes(1));
            _workerTimers = new List<Task>();
        }

        private static TokenSettings GetTokenSettings()
        {
            var proxy = GetProxy();
            var token = new TokenSettings()
            {
                Proxy = proxy,
            };

            try
            {
                var q = DownloadItemsQueue.OrderByDescending(f => Guid.NewGuid()).FirstOrDefault();
                var slug = string.IsNullOrEmpty(q.slug) ? q.id.ToString() : q.slug;

                var url = $"https://market.yandex.ru/product--{slug}/{q.id}?local-offers-first=1&how=aprice&deliveryincluded=1&onstock=0&page=1";

                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
                req.Proxy = proxy == null ? null : new Apix.Http.Client.WebProxy(proxy);
                req.MaximumAutomaticRedirections = 3;
                req.AllowAutoRedirect = true;
                req.Headers.Add("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
                req.Headers.Add("Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                req.Headers.Add("Cache-Control: no-cache");
                req.Headers.Add("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36 Edg/80.0.361.69");
                req.Headers.Add("Upgrade-Insecure-Requests: 1");
                req.Headers.Add($"Cookie: {GetCookies()}");
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                using (StreamReader stream = new StreamReader(
                    resp.GetResponseStream(), Encoding.UTF8))
                {
                    var result = stream.ReadToEnd();

                    //wc.Headers["X-Requested-With"] = "XMLHttpRequest";
                    //wc.Headers["Content-Data"] = "MTQ1MjkxODU3OC5iYmQyMjE4ODcwYTlhM2UzZTIzMWRmY2ExOGUxOGJlMA==";

                    token.UID = GetCookies();
                    token.Token = GetToken(result);
                    if (string.IsNullOrEmpty(q.slug))
                    {
                        q.slug = GetSlug(result, q.id);
                        if (!string.IsNullOrEmpty(q.slug))
                        {
                            TaskService.SaveSlug(q.id, q.slug);
                        }
                    }
                    return token;
                }
            }
            catch (Exception ex)
            {
                Delay = Delay + 1;
                Console.WriteLine("[Exception]: {0}", ex.ToString());
                return null;
            }


        }

        private void SaveSlug(QueueItem q, ProxySettings proxy)
        {

            try
            {
                var url = $"https://market.yandex.ru/product--{q.id}/{q.id}?local-offers-first=1&how=aprice&deliveryincluded=1&onstock=0&page=1";

                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
                req.Proxy = proxy == null ? null : new Apix.Http.Client.WebProxy(proxy);
                req.MaximumAutomaticRedirections = 3;
                req.AllowAutoRedirect = true;
                req.Headers.Add("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
                req.Headers.Add("Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                req.Headers.Add("Cache-Control: no-cache");
                req.Headers.Add("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36 Edg/80.0.361.69");
                req.Headers.Add("Upgrade-Insecure-Requests: 1");
                if (string.IsNullOrEmpty(Token.UID))
                {
                    req.Headers.Add($"Cookie: {Token.UID}");
                }


                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                using (StreamReader stream = new StreamReader(
                    resp.GetResponseStream(), Encoding.UTF8))
                {
                    var result = stream.ReadToEnd();

                    //wc.Headers["X-Requested-With"] = "XMLHttpRequest";
                    //wc.Headers["Content-Data"] = "MTQ1MjkxODU3OC5iYmQyMjE4ODcwYTlhM2UzZTIzMWRmY2ExOGUxOGJlMA==";

                    q.slug = GetSlug(result, q.id);
                    if (!string.IsNullOrEmpty(q.slug))
                    {
                        TaskService.SaveSlug(q.id, q.slug);
                    }

                }
            }
            catch (Exception ex)
            {
                Delay = Delay + 1;
                Console.WriteLine("[Exception]: {0}", ex.ToString());
            }


        }

        #region Timers

        private void GetNewTask(object state)
        {

            try
            {
                Console.WriteLine("[adding] #{0:D02}", DownloadItemsQueue.Count);
                if (DownloadItemsQueue.Count > 50)
                    return;

                var tasks = TaskService.ListActive(Environment.MachineName, AppConfig.Location).Result.ToList();
                if (!tasks.Any())
                {
                    TaskService.ResetNotCompletedTask(Environment.MachineName, AppConfig.Location).Wait();
                }
                foreach (var queueItem in tasks)
                {
                    if (DownloadItemsQueue.All(item => queueItem.id != item.id)
                        && WorkedItems.All(item => queueItem.id != item.Key))
                    {
                        DownloadItemsQueue.Enqueue(queueItem);
                    }
                }
                Console.WriteLine("[added] #{0:D02}", DownloadItemsQueue.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void UpdateProxy(object state)
        {
            var list = DisableProxy.ToList().Select(s => s.Address);
            try
            {
                lock (dequeueProxyLock)
                {
                    ProxyItemsQueue = new ConcurrentQueue<ProxySettings>(AppConfig.Proxies.Shuffle().Where(d => list.All(l => l != d.Address)).AsEnumerable());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
        }

        public static ProxySettings GetProxy()
        {
            ProxySettings proxy = null;
            if (ProxyItemsQueue.Count > 0)
            {
                lock (dequeueProxyLock)
                {
                    if (ProxyItemsQueue.TryPeek(out proxy))
                        ProxyItemsQueue.TryDequeue(out proxy);

                }
                ProxyItemsQueue.Enqueue(proxy);
            }

            return proxy;
        }

        private object dequeueLock = new object();
        private static object dequeueProxyLock = new object();

        void TimerCallback()
        {

            int tid = Thread.CurrentThread.ManagedThreadId;
            QueueItem item;
            var run = false;
            lock (dequeueLock)
            {
                if (DownloadItemsQueue.TryPeek(out item))
                    if (WorkedItems.All(i => i.Key != item.id))
                        if (DownloadItemsQueue.TryDequeue(out item))
                            run = true;
            }
            if (item != null && run)
            {

                WorkedItems.AddOrUpdate(item.id, item, (i, queueItem) => item);
                Worker(item).Wait();
                Console.WriteLine("[{0:D02}]: Task  #{1} finished", tid, item.id);
                WorkedItems.TryRemove(item.id, out item);

            }

        }

        private async void Timer()
        {
            while (true)
            {
                await Task.Run(() => TimerCallback());
                await Task.Delay(TimeSpan.FromMilliseconds(10));
            }
        }

        #endregion

        public void RunJobs(int count)
        {
            if (_isRun)
                return;

            _isRun = true;
            for (int i = 0; i < count; i++)
            {
                var t = Task.Factory.StartNew(
                    Timer,
                    _cts.Token,
                    TaskCreationOptions.LongRunning,
                    TaskScheduler.Current
                );
                _workerTimers.Add(t);
            }
        }



        private static string GetToken(string html)
        {
            string pattern = @"\""sk\"":\""([a-zA-Z0-9]*)\""";
            var match = Regex.Match(html, pattern);

            return match.Groups[1].Value;
        }

        private static string GetSlug(string html, int id)
        {
            string pattern = $@"a class=""link n-smart-link i-bem"" href=""\/product--([a-zA-Z0-9-]*)\/{id}/offers";
            var match = Regex.Match(html, pattern);

            return match.Groups[1].Value;
        }


        private static string GetCookies()
        {
            return "yandexuid=3191881991587980235; yuidss=3191881991587980235; skid=6454721551587980235; visits=1587980235-1587980235-1587980235; uid=AABbhl6mp8sZhQBoKy54Ag==; js=1; dcm=1; mOC=1; first_visit_time=2020-04-27T12%3A37%3A16%2B03%3A00; ymex=1903340236.yrts.1587980236; _ym_wasSynced=%7B%22time%22%3A1587980236840%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; gdpr=0; _ym_uid=1587980237809383489; _ym_d=1587980237; mda=0; ugcp=1; _ym_isad=2; _ym_visorc_45411513=b; _ym_visorc_160656=b; fonts-loaded=1; _ym_visorc_784657=b; oMaSefD=1; Session_id=3:1587980267.5.0.1587980267382:E0aq1Q:43.1|1071895015.0.2|216155.291727.g8kJjHrT8Gs0ONaeF61cq1LPqLw; sessionid2=3:1587980267.5.0.1587980267382:E0aq1Q:43.1|1071895015.0.2|216155.594159.HJBMwNFQWKzqM9dswc9n6At2H_c; yp=1903340267.udn.cDpzZS5hbG1hem92; ys=udn.cDpzZS5hbG1hem92; L=YVBVXUVaD2JSQ1JfBFRjeQRgQEBNAFh5KjdWDjYHCTweQw==.1587980267.14216.332747.06cd90ec25df6868dca007ea0022dc44; yandex_login=se.almazov; i=cUDOeefC4HhRngY0Q4cGLm5rnC4ohmhjuSCxvQ84CZL1hMTTVZ+2f7E9hJeo8OsEpKEMvIKdMWVsh9ZV+7gkp+awWKk=; oMaSpfD=1; oMaRefD=1; oMaPrfD=1; parent_reqid_seq=24c8823fa5ecef96ec4e6908338b9c5b%2C548c57f14fa7ae6404b5208de2ddcc6a%2C452c16a6612d07102800249522a793e4%2C523b97ded1733982cec14840eb960337; HISTORY_AUTH_SESSION=b9990502";

            //CookieContainer gaCookies = new CookieContainer();

            //for (int i = 0; i < response.Count; i++)
            //{
            //    string name = response.GetKey(i);
            //    if (name != "Set-Cookie")
            //        continue;
            //    var value = response.Get(i).Replace(".null", ".market.yandex.ru");
            //    gaCookies.SetCookies(new Uri("http://market.yandex.ru"), value);

            //    var cookieCol = gaCookies.GetCookies(new Uri("https://market.yandex.ru"));
            //    var result = new StringBuilder();
            //    foreach (Cookie cook in cookieCol)
            //    {
            //        if (!cook.Name.Contains("currentRegion"))
            //            result.Append($"{cook.Name}={cook.Value}; ");

            //    }
            //    return result.ToString();
            //}


            //return null;
        }

        public async Task Worker(QueueItem queue)
        {
            await Task.Delay(TimeSpan.FromSeconds(Delay));

            List<Offer> offers;
            var token = Token;
            var settings = token?.Proxy;


            if (token == null || string.IsNullOrEmpty(token.Token))
            {
                Console.WriteLine("[market #{0:D02} proxy:{1}]: Token not found", queue.id, settings?.Address);
                queue.updated_date = DateTime.UtcNow.AddHours(1);
                queue.parsed = 10;
                Token = null;
                Delay = Delay + 60;
                await TaskService.Close(queue);
                return;
            }

            if (string.IsNullOrEmpty(queue.slug))
            {
                SaveSlug(queue, settings);
                Console.WriteLine("[market #{0:D02} proxy:{1}]: slug not found", queue.id, settings?.Address);
                queue.updated_date = DateTime.UtcNow.AddHours(1);
                queue.parsed = 10;
                Token = null;
                Delay = Delay + 60;
                await TaskService.Close(queue);
                return;
            }

            try
            {
                switch ((Region)queue.location)
                {
                    case Region.Moscow:
                        offers = await new YaMarketClient(token.UID, 2, token.Token, settings).ListOffers(queue.id, queue.slug, 20, CancellationToken.None);
                        Console.WriteLine("[market #{0:D02}]: Found {1} offers in location #{2}", queue.id,
                            offers.Count, Region.Moscow);
                        break;
                    default:
                        offers = await new YaMarketClient(token.UID, 2, token.Token, settings).ListOffers(queue.id, queue.slug, 20, CancellationToken.None);
                        Console.WriteLine("[market #{0:D02}]: Found {1} offers in location #{2}", queue.id,
                            offers.Count, Region.Spb);
                        break;
                }
                //await CacheService.Save(queue.id, offers);

            }
            catch (Exception ex)
            {
                Token = null;
                Delay = Delay + 1;

                Console.WriteLine("[market #{0:D02} proxy:{1} {3}]: {2}", queue.id, settings?.Address, ex.ToString(), ex.GetType().Name);
                if (ex.GetType().Name == "HttpNotFoundException")
                {
                    queue.updated_date = DateTime.UtcNow.AddDays(5);
                    queue.parsed = 3;
                    await TaskService.Close(queue);
                    return;
                }
                if (ex.GetType().Name == "HttpRequestException")
                {
                    queue.updated_date = DateTime.UtcNow.AddDays(3);
                    queue.parsed = 4;
                    await TaskService.Close(queue);
                    return;
                }
                if (ex.GetType().Name == "HttpUnauthorizedException")
                {

                    if (settings == null)
                    {
                        DownloadItemsQueue.Clear();
                        DisableProxy = new ConcurrentBag<ProxySettings>();
                        _newTaskTimer = new Timer(GetNewTask, null, TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(1));

                    }
                    else
                    {
                        DisableProxy.Add(settings);
                    }
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    await TaskService.Reset(queue);
                    return;
                }
                await TaskService.Reset(queue);


                return;
            }
            try
            {
                Delay = 1;
                //await offers.ForEachAsyncWithExceptions(4, async offer =>
                //{
                //    int index = offers.FindIndex(a => a.Id == offer.Id);
                //    await TaskService.SaveOffer(queue.id, index, queue.location, offer);
                //});
                var toSave = offers.OrderBy(offer => offer?.ShowUid).Select(offer => new OfferModel()
                {
                    MarketId = queue.id,
                    Location = queue.location,
                    position = offers.FindIndex(a => a.ShowUid == offer.ShowUid),
                    shop_name = offer?.Shop?.Name,
                    shop_id = (int?)offer?.Shop?.Id ?? 0,
                    delivery_price = string.IsNullOrEmpty(offer?.Delivery?.Price?.Value) ? (int?)null : int.Parse(offer?.Delivery?.Price?.Value),
                    price = (int?)offer?.Prices?.RawValue ?? (int?)offer?.Seller?.Price,
                    warranty = false,
                    ratings = 0

                }).ToList();
                if (toSave.Any())
                {
                    await new ApiClient(AppConfig.ApiUrl).SaveOffers(queue.id, queue.location, toSave, CancellationToken.None);
                }
                else
                {
                    queue.updated_date = DateTime.UtcNow.AddDays(3);
                    queue.parsed = 5;
                    await TaskService.Close(queue);
                }

                //foreach (var offer in offers)
                //{
                //    int index = offers.FindIndex(a => a.Id == offer.Id);
                //    TaskService.SaveOffer(queue.id, index, queue.location, offer).Wait();
                //}
                //queue.parsed = 2;
                //queue.updated_date = DateTime.UtcNow;
                //await TaskService.Close(queue);

            }
            catch (Exception ex)
            {
                Console.WriteLine("[market #{0:D02} proxy:{1}]: {2}", queue.id, settings?.Address, ex.ToString());
            }

        }

        public void Stop()
        {
            if (_isRun)
            {
                _cts.Cancel();
                _isRun = false;
            }
        }
    }
}
