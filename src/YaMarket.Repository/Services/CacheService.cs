﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Apix.Sync.YaMarket.Models;
using YaMarket.Repository.Repositories;

namespace YaMarket.Repository.Services
{
    public static class CacheService
    {
        public static  Task Save(int id, List<Offer> offers)
        {
            return new CacheRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).SaveInfo(id, offers);
        }

        public static  Task<List<Offer>> GetOffers(int id)
        {
            return  new CacheRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]).GetInfo(id);
        }
    }
}