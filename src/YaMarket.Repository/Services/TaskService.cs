﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Apix.Sync.YaMarket.Models;
using YaMarket.Repository.Models;
using YaMarket.Repository.Repositories;

namespace YaMarket.Repository.Services
{
    public static class TaskService
    {
        public static TaskRepository Repository => new TaskRepository(AppConfig.Config["DATABASE_CONNECTION_STRING"]);
        public static  Task<IEnumerable<QueueItem>> ListActive(string envname, int? region = null)
        {
            return  Repository.ListActiveTask(envname, region);
        }
        public static Task ResetNotCompletedTask(string envName, int? region = null)
        {
            return Repository.ResetNotCompletedTask(envName, region);
        }

  

        public static Task Close(QueueItem queue)
        {
            return Repository.Close(queue);
        }

        public static Task SaveSlug(int id, string slug)
        {
            return Repository.SaveSlug(id, slug);
        }

        public static Task Reset(QueueItem queue)
        {
            return Repository.Reset(queue);
        }

        public static  Task Insert(int id, int location)
        {
            return Repository.Insert(id, location);
        }
        public static Task InsertSoft(int id, int location)
        {
            return Repository.InsertSoft(id, location);
        }
        public static Task ResetAll( int location)
        {
            return Repository.ResetAll( location);
        }

        
        public static  Task Insert(int id, int location, int productId)
        {
            return Repository.Insert(id, location, productId);
        }


    }
}
