﻿using System.Collections.ObjectModel;
using Apix.Http.Client;

namespace YaMarket.Repository.Infrastructure
{
    public class ProxyConfiguration
    {
        public Collection<ProxySettings> Proxies { get; set; }
        public bool UseProxy { get; set; }
    }
}
